---
pagetitle: "Funciones en lógicas"
title: "Semana 5: Funciones en lógicas"
subtitle: Taller de Excel | [ECON-1300](https://bloqueneon.uniandes.edu.co/d2l/home/133085)
author: 
      name: Eduard-Martínez
      affiliation: Universidad de los Andes  #[`r fontawesome::fa('globe')`]()
# date: Lecture 10  #"`r format(Sys.time(), '%d %B %Y')`"
output: 
  html_document:
    theme: flatly
    highlight: haddock
    # code_folding: show
    toc: yes
    toc_depth: 4
    toc_float: yes
    keep_md: false
    keep_tex: false ## Change to true if want keep intermediate .tex file
    ## For multi-col environments
  pdf_document:
    latex_engine: xelatex
    toc: true
    dev: cairo_pdf
    # fig_width: 7 ## Optional: Set default PDF figure width
    # fig_height: 6 ## Optional: Set default PDF figure height
    includes:
      in_header: tex/preamble.tex ## For multi-col environments
    pandoc_args:
        --template=tex/mytemplate.tex ## For affiliation field. See: https://bit.ly/2T191uZ
always_allow_html: true
urlcolor: blue
mainfont: cochineal
sansfont: Fira Sans
monofont: Fira Code ## Although, see: https://tex.stackexchange.com/q/294362
## Automatically knit to both formats
---

```{r setup, include=F , cache=F}
# load packages
require(pacman)
p_load(here,knitr,tidyverse,ggthemes,fontawesome,kableExtra)

# option html
options(htmltools.dir.version = F)
opts_chunk$set(fig.align="center", fig.height=4 , dpi=300 , cache=F)
```

<!--==================-->
<!--==================-->
## **[1.] Condicionales:**

Puede acceder a la documentación completa de las funciones lógicas en el [Soporte de Microsoft](https://support.microsoft.com/es-es/office/funciones-lógicas-referencia-e093c192-278b-43f6-8c3a-b6ce299931f5). Antes de hablar de las funciones, debería conocer algunos de los operadores lógicos en Excel:

```{r,echo=F}
my_tbl = tribble(
~`Operador lógico`,~Descripción,
"< , >","Menor y mayor que...",
"<= , >=", "Menor o igual y mayor igual que..." ,
"`=`" , "Igual a..." ,
"`<>`" , "Diferente de...")

kbl(my_tbl) %>%
kable_paper("striped", full_width = F) %>%
row_spec(0, bold = T, color = "white", background = "#00008B")
```

<!--------------------->
### **1.1 Función** `SI()`

La función SI es una de las funciones más populares de Excel y le permite realizar comparaciones lógicas entre un valor y un resultado que espera. Por esto, una instrucción SI puede tener dos resultados. El primer resultado es si la comparación es Verdadera y el segundo si la comparación es Falsa.

```{r, eval=F}
SI("C. Lógica 1";"[Verdadero]";"[Falso]")
```

Los argumentos de la función son **C. Lógica 1** condición lógica que se debe cumplir (necesario). **Verdadero** es el valor que debe retornar si cumple la condición dentro de **[C. Lógica 1]** (opcional). **Falso** es el valor que debe retornar si NO se cumple la condición dentro de **[C. Lógica 1]** (opcional).

<!------------------------>
### **1.2 Función** `Y()`

Puede usar `Y`, una de las funciones lógicas, para determinar si todas las condiciones de una prueba son VERDADERAS.

```{r, eval=F}
Y("C. Lógica 1";"[C. Lógica 2]";...;"[C. Lógica n]")
```

Los argumentos de la función son **C. Lógica 1** condición lógica 1 que se debe cumplir. **C. Lógica n** condición lógica n que se debe cumplir. Las condiciones están concatenadas por la condición **Y**, es decir se deben cumplir todas las condiciones para que el resultado sea verdadero.

<!------------------------>
### **1.3 Función** `O()`

```{r, eval=F}
O("C. Lógica 1";"[C. Lógica 2]";...;"[C. Lógica N]")
```

Los argumentos de la función son **C. Lógica 1** condición lógica 1 que se debe cumplir. **C. Lógica n** condición lógica n que se debe cumplir. Las condiciones están concatenadas por la condición **O**, es decir si se cumple a lo menos una de las condiciones, el resultado es verdadero.

<!--==================-->
<!--==================-->
## **[2.] Funciones anidadas:**

![](pics/f_anidada.png){width=40%}

Usar una función como uno de los argumentos en una fórmula de que usa una función se denomina anidamiento, y nos referiremos a esta función como una función anidada. Por ejemplo, se pueden anidar las funciones PROMEDIO y SUMA en los argumentos de la función SI, en una fórmula que suma un conjunto de números solo si el promedio de otro conjunto de números es mayor que determinado valor. De lo contrario, devuelve 0.

<!------------------------>
### **2.1 Función** `SI()` **dentro de otra función** `SI()`

```{r, eval=F}
SI("C. Lógica";"[Verdadero]";SI("C. Lógica 2";"[Verdadero]";"[Falso]"))
```

Por ejemplo, **C. Lógica** es la condición lógica que se debe cumplir (necesaria). **Verdadero** es el valor que debe retornar si cumple la condición dentro de **C. Lógica** (opcional). En el argumento **Falso** se genera **C. Lógica 2** que es la condición lógica que se debe cumplir si NO se cumple la primera condición.

<!------------------------>
### **2.1 Funciones** `Y()` o `O()` **dentro de una función** `SI()`

```{r, eval=F}
SI(Y("C. Lógica 1";"[C. Lógica 2]";...);"[Verdadero]";"[Falso]")
SI(O("C. Lógica 1";"[C. Lógica 2]";...);"[Verdadero]";"[Falso]")
```

Intente descomponer está función en 2 partes, primero `Y("C. Lógica 1";"[C. Lógica 2]";...)` que es el grupo de condiciones lógicas que se deben cumplir (necesaria). Después viene el argumento **Verdadero** que es el valor que debe retornar si cumplen todas las condición dentro de `Y()` (opcional). Finalmente tenemos **Falso** que es el valor que debe retornar si a lo menos una de las condición dentro de `Y()` no se cumple (opcional). Nótese que si solo se usa una condición lógica dentro de `Y()`, es equivalente a usar la función `SI()`. Algo similar pasa con la función, dónde **Verdadero** es el valor que debe retornar si cumple a lo menos una de las condiciones dentro de `O()` (opcional). **Falso** es el valor que debe retornar si NO se cumple ninguna una de todas las condición dentro de `O()` (opcional).

<!--==================-->
<!--==================-->
## **[3.] Funciones para varias condiciones**

<!------------------------>
### **3.1 SI.CONJUNTO()**

La función SI.CONJUNTO comprueba si se cumplen una o varias condiciones (permite probar hasta 127 condiciones diferentes) y devuelve un valor que corresponde a la primera condición TRUE. SI.CONJUNTO puede sustituir a varias instrucciones SI.CONJUNTO anidadas y es más fácil de leer con varias condiciones.

```{r, eval=F}
SI.CONJUNTO("C. Lógica 1";"[Verdadero]";"[C. Lógica 2]";"[Verdadero]";...)
```

<!------------------------>
### **3.2 XO()**

La función XO() devuelve un valor lógico Exclusivo o de todos los argumentos.

```{r, eval=F}
XO("C. Lógica 1";"[Verdadero]";"[C. Lógica 2]";"[Verdadero]";...)
```

Sí **C. Lógica 1** y **C. Lógica 2** se evalúan como VERDADERO, entonces **XO()** devuelve FALSO. Sí **C. Lógica 1** y **C. Lógica 2** se evalúan como FALSO, entonces **XO()** devuelve FALSO. Al menos uno de los resultados de la prueba debe evaluarse como VERDADERO para devolver VERDADERO.

<!--==================-->
<!--==================-->
## **[5.] Task**

#### **1.**  Importe el archivo "datos_odc_2000_2020.xlsx" que está en Bloque Neón.
#### **2.** Rellene la variable "Hay cultivos?" con un valor igual 1 si hay cultivos en ese municipio-año y 0 sino.
#### **3.** Rellene la variable "Cultivos y Aspersión" con un valor igual 1  si hay cultivos y además se asperjó en ese municipio-año y 0 si no.
#### **4.** Rellene la variable "Erradicación o Aspersión" con un valor igual 1 si hubo erradicación o aspersión en ese municipio-año y 0 si no.
#### **5.** Rellene la variable "Departamento (47)" con un valor igual 1 si el municipio pertenece al Magdalena (código 47001:47999) y hubo coca en ese municipio-año.
#### **6.** Rellene la variable "Cultivos o Aspersión o Erradicación" con un valor igual 1 si hubo cultivos o erradicación o aspersión en ese municipio-año y 0 sino.
#### **7.** Rellene la variable "Cultivos y Aspersión y Erradicación" con un valor igual 1 si hubo cultivos, erradicación y aspersión en ese municipio-año y 0 sino.
#### **8.** Rellene la variable "Promedio aspersión" con el valor de la aspersión en ese municipio-año y sino rellénela con el promedio de las aspersiones en todo el país-años. 
#### **9.** Rellene las celdas P2, P3, P4 y P5 con la suma de municipios que cumplen la condición.
#### **10.** Guarde el libro en formato .xlsx, asígnele su código como nombre del archivo y súbalo a Bloque Neón. Por ejemplo: **201725842.xlsx**


